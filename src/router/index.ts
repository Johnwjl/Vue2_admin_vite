import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: import.meta.env.BASE_URL,
  routes: [
    // Home
    {
      path: '/',
      name: 'home',
      redirect: 'login'
    },
    // Login
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/account/AccountLogin.vue')
    }
    // { path: '*', component: NotFoundComponent }
  ]
})

export default router
